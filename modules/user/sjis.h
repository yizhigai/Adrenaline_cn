#ifndef __SJIS_H__
#define __SJIS_H__

void initSJISIconv();
int convSJISToUTF16(unsigned short *dst, const char *src);
int convUTF16ToSJIS(char *dst, unsigned short src);
int convUTF8ToUTF16(unsigned short *dst, const char *src);
int convUTF16ToUTF8(char *dst, unsigned short src);
void convStringSJISToUTF8(char *dst_buf, const char *src_buf);
void convStringUTF8ToSJIS(char *dst_buf, const char *src_buf);

#endif

